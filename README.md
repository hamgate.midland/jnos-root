# Midland Hamgate /jnos root

This repository contains the mirror of files stored in the operational
directory (/jnos) of the Midland hamgate.  There are multiple branches in
this repository; production files are found in `production`.

Branches:
* **master** - nothing here except .gitignore and README
* **old-150427** - old computer contents
* **production** - current working copy
* **raspi-initial** - Initial Raspberry Pi image with no content

---

## NOTE

There is information about the current status on the wiki associated
with this repository.  Please review the information and add anything
you notice that is missing.

__It is a wiki__ - the whole point is that you can edit it, so
don't be shy about pressing the Edit button. :slight_smile:

---

If your screen is small, you might not be able to see the text
associated with the icons on the gray bar on the left.

The icon near the top that looks like two pieces of paper takes
you to the **files** in this repository.  You can view individual files,
or download a zip file of all the files.

The icon near the bottom that looks like a book leads to the **wiki**.

Depending on where you are, not all icons are visible.

---

**Menu with text shown**

![GitLab menu](GitLab_menu.jpg)

 
---

## Making changes

If you make a configuration change, do the following (on the hamgate):
```bash
git checkout production
```

make your changes

```bash
git add -A
git commit -m "short description of changes"
git push origin production
```

:no_entry_sign: **Do not leave another branch checked out on the hamgate**

The `production` branch is the only branch with the correct files.

---

If you have git installed on your home machine, you may clone this repository, 
make changes locally, push your changes, and then, on the hamgate, pull the 
changes down from this repository.

---

